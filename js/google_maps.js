/**
 * Created by ruan on 2017/01/16.
 */
//Controller for the google maps tab
define(["angular", "./App_routes"], function (ng) {
    "use strict";

    ng.module.controller("googleCtrl", function($scope) {
        $scope.msg = "Google Maps";


        function myMap() {
            var mapCanvas = document.getElementById("map");
            var mapOptions = {
                center: new google.maps.LatLng(-28.5 , 24),
                zoom: 6
            };
            var map = new google.maps.Map(mapCanvas, mapOptions);
        };

        myMap();

    });
});



