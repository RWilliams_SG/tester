/**
 * Created by ruan on 2017/01/17.
 */
define(["angular", "./App_routes"], function (ng) {
    "use strict";

    ng.module.controller("welkomCtrl", function($scope) {
        $scope.msg = "Test welkom page controller"
    });
});
