/**
 * Created by ruan on 2017/01/17.
 */
// var Highcharts = require('highcharts');  back end
require.config({
    "baseUrl": "js",
    "paths": {
        "angular": "../bower_components/angular/angular",
        "highcharts": "../bower_components/highcharts/highcharts"
    },
    "shim": {
        "angular": {
        "exports": "angular"
        },
        "ngRoute": {
            "deps": [
                "angular"
            ]
        },
        "highcharts": {
            "deps": [
                "angular"
            ]
        }
    }
});


require([
    "angular",
    "highcharts"
], function (angular) {
    "use strict";
    angular.element(document).ready(function () {

    })
});

