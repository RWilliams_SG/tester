/**
 * Created by ruan on 2017/01/16.
 */
define(["angular"], function (ng) {
    "use strict";

    ng.module("app", []);

    var module = ng.module("myApp", ["ngRoute"]);

    module.config(function($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl : "./html/Welkom.html",
                controller : "welkomCtrl"
            })
            .when("/Google_Maps", {
                templateUrl : "./html/Google_Maps.html",
                controller : "googleCtrl"
            })
            .when("/Leaflet_Maps", {
                templateUrl : "./html/Leaflit_Maps.html",
                controller : "leafletCtrl"
            })
            .when("/Scatter_Plot", {
                templateUrl : "./html/Scatter_Plot.html",
                controller : "ScatterCtrl"
            })
            .when("/AdvPlot", {
                templateUrl : "./html/advplot.html",
                controller : "advplotCtrl"
            })
            .when("/Formpage", {
                templateUrl : "./html/Formpage.html",
                controller : "advplotCtrl"
            })

    });
    return module;
});

/*var app = angular.module("myApp", ["ngRoute"]);

app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "./html/Welkom.html",
            controller : "welkomCtrl"
        })
        .when("/Google_Maps", {
            templateUrl : "./html/Google_Maps.html",
            controller : "googleCtrl"
        })
        .when("/Leaflet_Maps", {
            templateUrl : "./html/Leaflit_Maps.html",
            controller : "leafletCtrl"
        })
        .when("/Scatter_Plot", {
            templateUrl : "./html/Scatter_Plot.html",
            controller : "ScatterCtrl"
        })
        .when("/AdvPlot", {
            templateUrl : "./html/advplot.html",
            controller : "advplotCtrl"
        })
        .when("/Formpage", {
            templateUrl : "./html/Formpage.html",
            controller : "advplotCtrl"
        })

});

app.controller("welkomCtrl", function($scope) {
    $scope.msg = "Test welkom page controller"
});*/