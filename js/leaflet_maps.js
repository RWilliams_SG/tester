/**
 * Created by ruan on 2017/01/16.
 */

define(["angular", "./App_routes"], function (ng) {
    "use strict";

    ng.module.controller("leafletCtrl", function($scope) {
        $scope.msg = "Leaflet Interactive Maps";

        function leafletmaps() {
            var myLeaflet = L.map("mapid").setView([-28.5 , 24], 6);

            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="http://mapbox.com">Mapbox</a>',
                id: 'mapbox.comic'
            }).addTo(myLeaflet);
        };

        leafletmaps();
    });
});


/*
app.controller("leafletCtrl", function($scope) {
    $scope.msg = "Leaflet Interactive Maps";

    function leafletmaps() {
        var myLeaflet = L.map("mapid").setView([-28.5 , 24], 6);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="http://mapbox.com">Mapbox</a>',
            id: 'mapbox.comic'
        }).addTo(myLeaflet);
    };

    leafletmaps();
});*/